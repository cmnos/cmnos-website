<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class News extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('news', function (Blueprint $table) {
        $table->id();
        $table->bigInteger("author")->unsigned();
        $table->foreign("author")->references("id")->on('users');
        $table->string("title");
        $table->text("short")->nullable(); //this midnight be optional
        $table->text('image')->nullable(); //this midnight not be nullable and be required
        $table->enum('type', [
          "News"
        ]);
        $table->longText("text");
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
