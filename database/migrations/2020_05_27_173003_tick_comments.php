<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TickComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ticket_comments', function (Blueprint $table) {
        $table->id();
        $table->bigInteger("ticket_id")->unsigned();
        $table->foreign("ticket_id")->references("id")->on('ticket');
        $table->bigInteger('author')->unsigned();
        $table->foreign("author")->references("id")->on('users');
        $table->longText("messages");
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('ticket_comments');
    }
}
