<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Tick extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ticket', function (Blueprint $table) {
        $table->id();
        $table->bigInteger("author")->unsigned();
        $table->foreign("author")->references("id")->on('users');
        $table->string("title");
        $table->enum('priority', [
          "Low",
          "Medium",
          "High",
          "Important",
          "Critical",
          "Severe"
        ])->default('Low');
        $table->enum("status",[
          "OPEN",
          "CLOSED"
        ]);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('ticket');
    }
}
