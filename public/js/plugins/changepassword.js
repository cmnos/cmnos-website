$(document).ready(function () {
    $("#pw_change").on('click', function (e) {
        e.preventDefault();
        //$("#pw_change").prop('disabled', true);
        $.ajax({
            url: $("#pw_change").data('url'),
            data: {
                'oldpassword': $('#oldPassword').val(),
                'password': $('#newpassword').val(),
                'password_confirmed': $('#confirmnewpassword').val(),
                '_token': $('input[name="_token"]').val()
            },
            type: "POST",
            success: function (data) {
                var result = JSON.parse(atob(data));
                toastr[result.type](result.msg, result.title);
                if(result.type === "success")
                    $('#ChangePasswordModal').modal('toggle')
                $("#pw_change").prop('disabled', false);
            },
            error: function (data) {
                toastr.error('Unknow error had happens. Please contact game support.');
            }
        });
    });
});