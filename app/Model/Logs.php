<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Logs extends Model {
  public static function add($message, $level = "INFO", $context = null, $extra = null) {
    DB::table('logs')->insert([
      'message'=> $message,
      'level' => $level,
      'context' => $context,
      'extra' => $extra
    ]);
  }
}