<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\ticket_comments;
use App\ticket;

class TicketController extends Controller
{
    public function __construct() {
      $this->middleware('auth');
    }

    private function reg_ticketid($args) {
      $ticket = Auth::user()->tickets()->create([
        "title"   => $args->title,
        "status"  => "OPEN"
      ]);
      $ticket->messages()->create([
        "messages" => $args->msg,
        "author"   => $args->id
      ]);
      $ticket->save();
      return true;
    }

    public function create_ticket(Request $request) {
      $this->reg_ticketid((object)[
        "id"    => Auth::user()->id,
        "title" => $request->title,
        "msg"   => base64_encode($request->ticket_msg)
      ]);
      return response()->redirectTo('support');
    }

    private function get_replay_count($id) {
      return ticket_comments::where('ticket_id', $id)->count();
    }


    private function check_is_ownreply($aid, $lid) {
      return $aid !== $lid ? "Awaiting reply" : "Staff has replyed";
    }

    public function display_own(Request $request) {
      $uid =  Auth::user()->id;
      $tickets = ticket::where("author", $uid)->orderBy("id", 'DESC')->get();
      if(count($tickets) === 0) {
        return "<tr><td colspan='4' class='text-center text-white'>You don't have any ticket</td></tr>";
      }
      $tr="";
      for($i=0, $iMax = count($tickets); $i< $iMax; $i++) {
        $tr .="<tr><td><a class='btn btn-sm btn-primary' href='".url('/support/ticket/'.$tickets[$i]->id)."'>".$tickets[$i]->title."</a> (".$this->get_replay_count($tickets[$i]->id).")</td>
               <td>".$tickets[$i]->status."</td>
               <td>".$this->check_is_ownreply($uid, ticket_comments::where('ticket_id', $tickets[$i]->id)->orderBy('id','DESC')->pluck('author'))."</td>
               <td>".$tickets[$i]->created_at."</td></tr>";
      }
      return $tr;
    }

    public function addreply(Request $request) {
      $ticket = ticket::find($request->ticket_id);
      if($request->action === "add"){
        if(ticket::find($request->ticket_id)->author === Auth::user()->id || Auth::user()->access > 2) {
          $ticket = ticket_comments::create([
            "ticket_id" => $request->ticket_id,
            "author"    => Auth::user()->id,
            "messages"  => base64_encode($request->reply_msg),
          ]);
          $ticket->save();
          return response()->redirectTo('/support/ticket/'.$request->ticket_id);
        }
      }
      else {
        $ticket->status = "CLOSED";
        $ticket->save();
        return response()->redirectTo('support');
      }
    }

    public function viewticket(Request $request) {
      return ticket::find($request->id)->author === Auth::user()->id || Auth::user()->access > 2 ? view('ticket.ticketinside', [
        'tid'     => $request->id,
        'ticket'  => ticket::find($request->id),
        'replays' => ticket_comments::where('ticket_id', $request->id)->orderBy('id', 'ASC')->get(),
      ]) : response()->redirectTo('support');
    }

    public function display(Request $request) {
      if(Auth::user()->access > 2)
        return view('ticket.ticketadmin', [
          "tickets_open"    => ticket::where('status','OPEN')->orderBy('id', 'DESC')->get(),
          "tickets_closed"  => ticket::where('status','CLOSED')->orderBy('id', 'DESC')->get(),
        ]);
      else
        return view('ticket.tickethome', [
          "tickets" => $this->display_own($request)
        ]);
    }
}
