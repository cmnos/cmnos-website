<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GameAuthController;
use App\Logs;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use \Auth;

class ChangePwdController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

  use AuthenticatesUsers;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function updatepassword(Request $request) {
    $gs = new GameAuthController();
    $validator = Validator::make([
      "oldpassword"=>$request->oldpassword,
      "password"=> $request->password,
      "password_confirmation"=> $request->password_confirmed
    ], [
      'oldpassword' => ['required', 'string'],
      'password' => ['required', 'string', 'min:8', 'confirmed'],
    ])->getMessageBag();
    if(empty($validator->getMessages())) {
      $user = User::find(Auth::user()->id);
      if(password_verify($request->oldpassword, $user->password)) {
        Logs::add('Change password success','AUTH', $user->username);
        $user->password = Hash::make($request->password);
        $user->save();
        $gs->setPassword($user->email, $request->password);
        return response()->json(base64_encode(json_encode([
          "type"  => "success",
          "title" => "Password Changed",
          "msg"   => "You'r password has been updated success fully"
        ])));
      }
      else {
        Logs::add('Change password failed, wrong current password','AUTH', Auth::user()->username);
        return response()->json(base64_encode(json_encode([
          "type" => "error",
          "title" => "Wrong password",
          "msg" => "You'r current password doesn't match our database. Please try again"
        ])));
      }
    }
    else {
      Logs::add('Change password failed, wrong password validation','INFO', Auth::user()->username);
      return response()->json(base64_encode(json_encode([
        "type" => "error",
        "title" => "Password aren't maching",
        "msg" => "Double check you'r password before send."
      ])));
    }
  }
}
